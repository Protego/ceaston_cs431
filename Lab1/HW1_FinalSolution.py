import numpy as np
import copy
import sys


solutions = list()

def topLeft(board):
    tot = 0
    if board[1,0] == 9:
        tot+=1
    if board[1,1] == 9:
        tot+=1
    if board[0,1] == 9:
        tot+=1
    return tot
    
def topRight(board,cmax):
    tot = 0
    if board[cmax-1,0] == 9:
        tot+=1
    if board[cmax-1,1] == 9:
        tot+=1
    if board[cmax,1] == 9:
        tot+=1
    return tot

def botLeft(board,rmax):
    tot = 0
    if board[1,rmax] == 9:
        tot+=1
    if board[1,rmax-1] == 9:
        tot+=1
    if board[0,rmax-1] == 9:
        tot+=1
    return tot
    
def botRight(board,cmax,rmax):
    tot = 0
    if board[cmax-1,rmax] == 9:
        tot+=1
    if board[cmax-1,rmax-1] == 9:
        tot+=1
    if board[cmax,rmax-1] == 9:
        tot+=1
    return tot

def right(board,cmax,r):
    tot = 0
    if board[cmax-1,r] == 9:
        tot+=1
    if board[cmax-1,r-1] == 9:
        tot+=1
    if board[cmax,r-1] == 9:
        tot+=1
    if board[cmax-1,r+1] == 9:
        tot+=1
    if board[cmax,r+1] == 9:
        tot+=1
    return tot
    
def left(board,r):
    tot = 0
    if board[1,r] == 9:
        tot+=1
    if board[1,r-1] == 9:
        tot+=1
    if board[0,r-1] == 9:
        tot+=1
    if board[1,r+1] == 9:
        tot+=1
    if board[0,r+1] == 9:
        tot+=1
    return tot

def bot(board,c,rmax):
    tot = 0
    if board[c,rmax-1] == 9:
        tot+=1
    if board[c-1,rmax-1] == 9:
        tot+=1
    if board[c+1,rmax-1] == 9:
        tot+=1
    if board[c+1,rmax] == 9:
        tot+=1
    if board[c-1,rmax] == 9:
        tot+=1
    return tot
    
def top(board,c):
    tot = 0
    if board[c,1] == 9:
        tot+=1
    if board[c-1,1] == 9:
        tot+=1
    if board[c+1,1] == 9:
        tot+=1
    if board[c+1,0] == 9:
        tot+=1
    if board[c-1,0] == 9:
        tot+=1
    return tot

def center(board,c,r):
    tot = 0
    if board[c,r+1]==9:
        tot+=1
    if board[c,r-1]==9:
        tot+=1
    if board[c+1,r+1]==9:
        tot+=1
    if board[c+1,r-1]==9:
        tot+=1
    if board[c+1,r]==9:
        tot+=1
    if board[c-1,r+1]==9:
        tot+=1
    if board[c-1,r-1]==9:
        tot+=1
    if board[c-1,r]==9:
        tot+=1
    return tot

def checkValid(board,cmax,rmax):
    c=0
    r=0
    while c<=cmax:
        while r<=rmax:
            if board[c,r]>=0 and board[c,r]<=8:
                tot = 0
                if c==0:
                    if r==0:
                        tot = topLeft(board)
                    if r==rmax:
                        tot = botLeft(board,rmax)
                    if r>0 and r<rmax:
                        tot = left(board,r)
                if c==cmax:
                    if r==0:
                        tot = topRight(board,cmax)
                    if r==rmax:
                        tot = botRight(board,cmax,rmax)
                    if r>0 and r<rmax:
                        tot = right(board,cmax,r)
                if c>0 and c<cmax:
                    if r==0:
                        tot = top(board,c)
                    if r==rmax:
                        tot = bot(board,c,rmax)
                    if r>0 and r<rmax:
                        tot = center(board,c,r)
                if tot> board[c,r]:
                    return False
            r+=1    
        c+=1
        
    
    return True
    
def isSolution(board,cmax,rmax):
    tot = 0
    for c in range(0,cmax+1):
        for r in range(0,rmax+1):
            if board[c,r]>=0 and board[c,r]<=8:
                if c==0:
                    if r==0:
                        tot = topLeft(board)
                    if r==rmax:
                        tot = botLeft(board,rmax)
                    if r>0 and r<rmax:
                        tot = left(board,r)
                if c==cmax:
                    if r==0:
                        tot = topRight(board,cmax)
                    if r==rmax:
                        tot = botRight(board,cmax,rmax)
                    if r>0 and r<rmax:
                        tot = right(board,cmax,r)
                if c>0 and c<cmax:
                    if r==0:
                        tot = top(board,c)
                    if r==rmax:
                        tot = bot(board,c,rmax)
                    if r>0 and r<rmax:
                        tot = center(board,c,r)
                if tot!= board[c,r]:
                    return False
                print(c,r)
                print(tot)
					
               
        
        
    
    return True


def solve(b1,b2,cm,rm,s):
    if isSolution(b2,cm,rm):
        dup = False
        for index in range(0,len(solutions)):
            if (solutions[index]==b2).all():
                print("duplicates solution")
                dup = True
                
        if dup == False:
            solutions.append(np.copy(b2))
            s+=1
        print(b2)
        print(s)
    else:
        for c in range(0,cm+1):
            for r in range(0,rm+1):
                if b2[c,r]==-1:
                    print("if")
                    b2[c,r] = 9
                    if checkValid(b2,cm,rm):
                        s = solve(b1,b2,cm,rm,s)
                    b2[c,r]=-1

                
    return s

def startState(board,cm,rm):
    if checkValid(board,cm,rm):
        print("In start state")
        s=0;
        s=solve(board,board,cm,rm,s)
        print(s," valid solutions")
    else:
        print("No valid States")
    
    return board


def convertToInt(x):
    """Helper function to be able to use either form
       of the input file: '-' or '-1'
    """
    if x == '-':
        return -1
    else:
        return int(x)

def main(filename):
    """No error checking or handling."""
    with open(filename,'r') as f:
        # first line is # of rows
        nrows = int(f.readline().strip())
        # next line is # of columns
        ncols = int(f.readline().strip())
        # now nrows of the board
        board = np.zeros((nrows,ncols),dtype=int)
        for i in range(nrows):
            board[i,:] = list(map(convertToInt,f.readline().split()))
    print(board)
    board = startState(board,ncols-1,nrows-1)
    print(board)


if __name__ == "__main__":
    """Expects filename as first argument.  """
    main(sys.argv[1])