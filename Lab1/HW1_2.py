import numpy as np
import sys




def topLeft(board):
    tot = 0
    if board[1,0] == 9:
        tot+=1
    if board[1,1] == 9:
        tot+=1
    if board[0,1] == 9:
        tot+=1
    return tot
    
def topRight(board,cmax):
    tot = 0
    if board[cmax-1,0] == 9:
        tot+=1
    if board[cmax-1,1] == 9:
        tot+=1
    if board[cmax,1] == 9:
        tot+=1
    return tot

def botLeft(board,rmax):
    tot = 0
    if board[1,rmax] == 9:
        tot+=1
    if board[1,rmax-1] == 9:
        tot+=1
    if board[0,rmax-1] == 9:
        tot+=1
    return tot
    
def botRight(board,cmax,rmax):
    tot = 0
    if board[cmax-1,rmax] == 9:
        tot+=1
    if board[cmax-1,rmax-1] == 9:
        tot+=1
    if board[cmax,rmax-1] == 9:
        tot+=1
    return tot

def right(board,cmax,r):
    tot = 0
    if board[cmax-1,r] == 9:
        tot+=1
    if board[cmax-1,r-1] == 9:
        tot+=1
    if board[cmax,r-1] == 9:
        tot+=1
    if board[cmax-1,r+1] == 9:
        tot+=1
    if board[cmax,r+1] == 9:
        tot+=1
    return tot
    
def left(board,r):
    tot = 0
    if board[1,r] == 9:
        tot+=1
    if board[1,r-1] == 9:
        tot+=1
    if board[0,r-1] == 9:
        tot+=1
    if board[1,r+1] == 9:
        tot+=1
    if board[0,r+1] == 9:
        tot+=1
    return tot

def bot(board,c,rmax):
    tot = 0
    if board[c,rmax-1] == 9:
        tot+=1
    if board[c-1,rmax-1] == 9:
        tot+=1
    if board[c+1,rmax-1] == 9:
        tot+=1
    if board[c+1,rmax] == 9:
        tot+=1
    if board[c-1,rmax] == 9:
        tot+=1
    return tot
    
def top(board,c):
    tot = 0
    if board[c,1] == 9:
        tot+=1
    if board[c-1,1] == 9:
        tot+=1
    if board[c+1,1] == 9:
        tot+=1
    if board[c+1,0] == 9:
        tot+=1
    if board[c-1,0] == 9:
        tot+=1
    return tot

def center(board,c,r):
    tot = 0
    if board[c,r+1]==9:
        tot+=1
    if board[c,r-1]==9:
        tot+=1
    if board[c+1,r+1]==9:
        tot+=1
    if board[c+1,r-1]==9:
        tot+=1
    if board[c+1,r]==9:
        tot+=1
    if board[c-1,r+1]==9:
        tot+=1
    if board[c-1,r-1]==9:
        tot+=1
    if board[c-1,r]==9:
        tot+=1
    return tot

def checkValid(board,cmax,rmax):
    c=0
    r=0
    tot = 0
    while c<=cmax:
        while r<=rmax:
            if board[c,r]>=0 and board[c,r]<=8:
                if c==0:
                    if r==0:
                        tot = topLeft(board)
                    if r==rmax:
                        tot = botLeft(board,rmax)
                    if r>0 and r<rmax:
                        tot = left(board,r)
                if c==cmax:
                    if r==0:
                        tot = topRight(board,cmax)
                    if r==rmax:
                        tot = botRight(board,cmax,rmax)
                    if r>0 and r<rmax:
                        tot = right(board,cmax,r)
                if c>0 and c<cmax:
                    tot = center(board,c,r)
                if tot> board[c,r]:
                    return False
            r+=1    
        c+=1
        
    
    return True
    
def isSolution(board,cmax,rmax):
    c=0
    r=0
    tot = 0
    isit = True
    while c<=cmax:
        while r<=rmax:
            if board[c,r]>=0 and board[c,r]<=8:
                if c==0:
                    if r==0:
                        tot = topLeft(board)
                    if r==rmax:
                        tot = botLeft(board,rmax)
                    if r>0 and r<rmax:
                        tot = left(board,r)
                if c==cmax:
                    if r==0:
                        tot = topRight(board,cmax)
                    if r==rmax:
                        tot = botRight(board,cmax,rmax)
                    if r>0 and r<rmax:
                        tot = right(board,cmax,r)
                if c>0 and c<cmax:
                    tot = center(board,c,r)
                if tot != board[c,r]:
                    isit =  False
            r+=1    
        c+=1
        
    
    return isit


def solve(b1,b2,cm,rm,s):
    if isSolution(b2,cm,rm):
        print(b2)
        s.append(b2[:])
        print(s)
        
    for c in range(0,cm+1):
        print("while1")
        for r in range(0,rm+1):
            print("while2")
            if b2[c,r]==-1:
                print("if1")
                b2[c,r] = 9
                if checkValid(b2,cm,rm):
                    print(c)
                    print(r)
                    print(b2)
                    s = solve(b1,b2,cm,rm,s)
                b2[c,r]=-1

                
    return s

def startState(board,cm,rm):
    if checkValid(board,cm,rm):
        print("In start state")
        s=[]
        s=solve(board,board,cm,rm,s)
        print("Valid solutions")
        print(s)
    else:
        print("No valid States")
        print(board)
    
    return


def convertToInt(x):
    """Helper function to be able to use either form
       of the input file: '-' or '-1'
    """
    if x == '-':
        return -1
    else:
        return int(x)

def main(filename):
    """No error checking or handling."""
    with open(filename,'r') as f:
        # first line is # of rows
        nrows = int(f.readline().strip())
        # next line is # of columns
        ncols = int(f.readline().strip())
        # now nrows of the board
        board = np.zeros((nrows,ncols),dtype=int)
        for i in range(nrows):
            board[i,:] = list(map(convertToInt,f.readline().split()))
    print(board)
    startState(board,ncols-1,nrows-1)


if __name__ == "__main__":
    """Expects filename as first argument.  """
    main(sys.argv[1])